#!/bin/sh
TERRAFORM_URL=https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip

wget $TERRAFORM_URL -O /tmp/terraform.zip
unzip /tmp/terraform.zip -d /usr/bin
chmod +x /usr/bin/terraform